import java.util.Scanner;

public class hashExample<Key, Value>{
    int hashSize = 30001;
    Value[] vals = (Value[]) new Object[hashSize];
    Key[] keys = (Key[]) new Object[hashSize];

    private int hash(Key key) {
        return (key.hashCode() & 0x7FFFFFFF) % hashSize;
    }

    public void put(Key key, Value value) {
        int i;
        for (i = hash(key); keys[i] != null; i = (i + 1) % hashSize)
            if (keys[i].equals(key))
                break;
        keys[i] = key;
        vals[i] = value;
    }

    public Value get(Key key){
        for (int i = hash(key); keys[i] != null; i = (i+1) % hashSize){
            if (key.equals(keys[i])){
                return vals[i];
            }
        }
        return null;
    }
    public static void main(String[] args) {
        hashExample<String, String> hash = new hashExample<>();
        Scanner ip = new Scanner(System.in);
        String key = ip.next();
        String value = ip.next();
        hash.put(key, value);

        String find = ip.next();
        if (hash.get(find)==null) {
            System.out.println("not found");
        }else {
            System.out.println(hash.get(find));
        }
    }
}